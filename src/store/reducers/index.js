import {combineReducers} from "redux";

import app from "./app/reducers";

const appReducers = combineReducers(
  {
    app
  }
);


export default appReducers;