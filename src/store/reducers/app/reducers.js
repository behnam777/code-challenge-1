import {
  GET_INITIAL_QUESTIONNAIRE,
  GENERATE_QUESTION,
  REMOVE_QUESTION,
  LAST_QUESTION,
  SUBMIT_ANSWER
} from "./";

const initial = {
  isFinished: false,
  isLoading: true,
  questionnaire: [],
  headlines: [],
  answers: {},
  totalOptions: 0,
  currentChoice: {first: {}, second: {}}
};

const data = (state = initial, action) => {
  switch (action.type) {
    //set isLoading true
    case GET_INITIAL_QUESTIONNAIRE:
      return {...state, isLoading: true};

    //fill required state's fields
    case `${GET_INITIAL_QUESTIONNAIRE}_FULFILLED`: {
      return {
        ...state,
        isLoading: false,
        questionnaire: action.payload.questionnaire,
        headlines: action.payload.headlines,
        answers: action.payload.answers,
        totalOptions: action.payload.totalOptions
      };
    }

    //set generated question in currentChoice
    case GENERATE_QUESTION:
      return {...state, currentChoice: {first: action.payload.optionA, second: action.payload.optionB}};

    //remove questions (in payload) from each related question category
    //increase headline of each category by one
    case REMOVE_QUESTION:
      return {
        ...state,
        questionnaire: state.questionnaire.map(category => {
          if (category.name === action.payload.questionA.name) {
            if (category.answers.length) {
              return {
                ...category,
                answers: category.answers.filter(answer => answer !== action.payload.questionA.question)
              }
            }
          }
          if (category.name === action.payload.questionB.name) {
            if (category.answers.length) {
              return {
                ...category,
                answers: category.answers.filter(answer => answer !== action.payload.questionB.question)
              }
            }
          }
          return category;
        }),
        headlines: state.headlines.map(headline => {
          if (headline.a === action.payload.questionA.name && headline.b === action.payload.questionB.name) {
            return {...headline, count: headline.count + 1}
          }
          return headline;
        })
      };

    //set is finished
    case LAST_QUESTION:
      return {...state, isFinished: true};

    //increase answer of related category by one
    case SUBMIT_ANSWER:
      if (!state.answers[action.payload]) {
        return {...state, answers: {...state.answers, [action.payload]: 1}};
      }
      return {...state, answers: {...state.answers, [action.payload]: state.answers[action.payload] + 1}};

    default:
      return state
  }
};

export default data;