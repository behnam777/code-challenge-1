import React from 'react';
import * as actions from './'
import reducer from './reducers';


describe('App reducer', () => {
  it('initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        isFinished: false,
        isLoading:true,
        questionnaire: [],
        headlines: [],
        answers: {},
        totalOptions:0,
        currentChoice: {first: {}, second: {}}
      }
    )
  });

  it('should handle last question', () => {
    expect(
      reducer([], {
        type: actions.LAST_QUESTION
      })
    ).toEqual(
      {
        isFinished: true
      }
    )
  });

  it('should handle is loading', () => {
    expect(
      reducer([], {
        type: actions.GET_INITIAL_QUESTIONNAIRE
      })
    ).toEqual(
      {
        isLoading: true
      }
    )
  });

  it('should handle initial questionnaire', () => {
    expect(
      reducer([], {
        type: actions.GET_INITIAL_QUESTIONNAIRE
      })
    ).toEqual(
      {
        isLoading: true
      }
    )
  });

  it('should handle questionnaire fulfilled', () => {
    const payload={
      questionnaire: [{name:'foo',answers:['1']},{name:'bar',answers:['2']}],
      headlines: ['foo','bar'],
      answers: {foo:0,bar:0},
      totalOptions:2
    };
    expect(
      reducer([], {
        type: `${actions.GET_INITIAL_QUESTIONNAIRE}_FULFILLED`,
        payload
      })
    ).toEqual(
      {
        ...payload,isLoading:false
      }
    )
  });


  it('should handle submit answer', () => {
    const payload='foo';
    expect(
      reducer({answers:{}}, {
        type: actions.SUBMIT_ANSWER,
        payload
      })
    ).toEqual(
      {
        answers :{'foo':1}
      }
    )
  });
});