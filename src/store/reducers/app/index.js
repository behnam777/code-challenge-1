import questionnaireJson from '../../../data/dimensions.json';
export const
  GET_INITIAL_QUESTIONNAIRE = 'APP/GET/QUESTIONNAIRE',
  GENERATE_QUESTION = 'APP/GENERATE/QUESTION',
  REMOVE_QUESTION = 'APP/REMOVE/QUESTION',
  LAST_QUESTION = 'APP/LAST/QUESTION',
  SUBMIT_ANSWER = 'APP/SUBMIT/ANSWER';

/**
 * fetch questionnaire - written with timeout to mock rest call
 * replace Promise with fetch()
 * @returns {{type: string, payload: Promise}}
 */
export const getInitialQuestionnaire = () => {
  return {
    type: GET_INITIAL_QUESTIONNAIRE,
    payload: new Promise((resolve) => {
      //mock api latency
      let wait = setTimeout(() => {
        clearTimeout(wait);
        let questionnaire = questionnaireJson;
        //extract headlines from questionnaire
        let headlines = extractHeadlines(questionnaire);
        //extract answers template
        let answers = extractAnswers(questionnaire);
        //extract total number of options
        let totalOptions = extractTotalOptions(questionnaire);
        //return
        resolve({questionnaire, headlines, answers, totalOptions});
      }, 500)
    })
  }
};

/**
 * returns a random headline from list of headlines
 * @param headlines
 * @returns {*}
 */
const getRandomCategory = (headlines) => {
  let length = headlines.length;
  if (length > 0) {
    let randIndex = Math.floor(Math.random() * length);
    return headlines[randIndex]
  }
  return null;
};

/**
 * return a random question from a category of questionnaire
 * @param category: name of the category
 * @param questionnaire: list of current questionnaire
 * @returns {null}
 */
const getRandomQuestion = (category, questionnaire) => {
  let chosenQuestionnaire = questionnaire.find(item => item.name === category);
  if (chosenQuestionnaire) {
    return chosenQuestionnaire.answers[Math.floor(Math.random() * chosenQuestionnaire.answers.length)];
  }
  return null;
};

/**
 * generate a question with two options randomly
 * @param questionnaire: list of current available questionnaire
 * @param headlines: list of all headlines
 * @returns {*}
 */
export const getQuestion = (questionnaire, headlines) => {
  if (headlines.length && questionnaire.length) {
    //filter headlines which number of currently generated questions are lower than 2
    let availableHeadlines = headlines.filter(item => item.count < 2);
    //get a random category
    let category = getRandomCategory(availableHeadlines);
    //if category founded
    if (category) {
      //get random question from each category
      let questionA = getRandomQuestion(category.a, questionnaire);
      let questionB = getRandomQuestion(category.b, questionnaire);
      if (questionA && questionB) {
        return {
          type: GENERATE_QUESTION,
          payload: {optionA: {name: category.a, question: questionA}, optionB: {name: category.b, question: questionB}}
        }
      }
    }
  }
  //if no category founded, it was last question
  return {
    type: LAST_QUESTION
  }
};

/**
 * remove a question with two options from questionnaire
 * @param questionA
 * @param questionB
 * @returns {{type: string, payload: {questionA: *, questionB: *}}}
 */
export const removeQuestionFromQuestionnaire = (questionA, questionB) => {
  return {
    type: REMOVE_QUESTION,
    payload: {questionA, questionB}
  }
};

/**
 * save the answer
 * @param answer
 * @returns {{type: string, payload: *}}
 */
export const submitAnswer = (answer) => {
  return {
    type: SUBMIT_ANSWER,
    payload: answer
  }
};

/**
 * iterate over questionnaire and its' categories and make all possible headlines
 * each headline should have two different options and all categories in questionnaire should match exactly once
 * @param questionnaire
 * @returns {Array}
 */
const extractHeadlines = (questionnaire) => {
  let headlines = [];
  if (questionnaire && Array.isArray(questionnaire) && questionnaire.length) {
    for (let i = 0; i < questionnaire.length; i++) {
      for (let j = i + 1; j < questionnaire.length; j++) {
        headlines.push({a: questionnaire[i].name, b: questionnaire[j].name, count: 0})
      }
    }
  }
  return headlines;
};

/**
 * Create empty template for answers
 * @param questionnaire
 * @returns {{}}: map of questionnaire's category keys
 */
const extractAnswers = (questionnaire) => {
  let answers = {};
  if (questionnaire && Array.isArray(questionnaire) && questionnaire.length) {
    questionnaire.forEach(item => answers[item.name] = 0)
  }
  return answers;
};

/**
 * extracts total number of options
 * @param questionnaire
 * @returns {number}
 */
const extractTotalOptions = (questionnaire) => {
  let result = 0;
  if (questionnaire && Array.isArray(questionnaire) && questionnaire.length) {
    questionnaire.forEach(item => result += item.answers.length);
  }
  return result;
};