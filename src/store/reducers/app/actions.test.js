import React from 'react';
import * as actions from './'
import reducer from './reducers';

describe('actions: submitAnswer', () => {
  it('should create an action to submit answer', () => {
    const payload = 'Some answer';
    const expectedAction = {
      type: actions.SUBMIT_ANSWER,
      payload
    };
    expect(actions.submitAnswer(payload)).toEqual(expectedAction)
  })
});

describe('actions: removeQuestionFromQuestionnaire', () => {
  it('should create an action to remove question', () => {
    const payload = {questionA:{name:'a',answer:'foo bar'},questionB:{name:'b',answer:'bar foo'}};
    const expectedAction = {
      type: actions.REMOVE_QUESTION,
      payload
    };
    expect(actions.removeQuestionFromQuestionnaire(payload.questionA,payload.questionB)).toEqual(expectedAction)
  })
});


describe('actions: getQuestion - empty', () => {
  it('should create an action to finish game', () => {
    const ques =[],head=[];
    const expectedAction = {
      type: actions.LAST_QUESTION
    };
    expect(actions.getQuestion(ques,head)).toEqual(expectedAction)
  })
});
