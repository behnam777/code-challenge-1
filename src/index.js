import createBrowserHistory from "history/createBrowserHistory"
import store from "./store";
import App from "./containers/App";
import Answer from "./containers/Answer";
import React from "react";
import ReactDOM from "react-dom";
import Result from "./containers/Result";
import {Provider} from "react-redux";
import {Router, Route, Switch} from "react-router-dom";

//create history
const history = createBrowserHistory();


ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={App}/>
        <Route exact path="/answer" component={Answer}/>
        <Route exact path="/result" component={Result}/>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
