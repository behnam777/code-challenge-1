import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './App.css';

class App extends React.Component {
  static propTypes = {
    getInitialQuestionnaire: PropTypes.func.isRequired,
    questionnaire: PropTypes.array,
    isLoading: PropTypes.bool
  };

  /**
   * On component mount, retrieve questionnaire
   */
  componentDidMount() {
    this.props.getInitialQuestionnaire();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Code Challenge</h1>
        </header>
        <p className="App-intro">
          Start by click on the button
        </p>
        <Link to="/answer">
          <button className="App-start" disabled={this.props.isLoading}>Start</button>
        </Link>
      </div>
    );
  }
}
export default App;