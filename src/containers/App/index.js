import App from './App';
import {connect} from 'react-redux';
import {getInitialQuestionnaire} from '../../store/reducers/app';

const mapStateToProps = (state) => ({
  questionnaire: state.app.questionnaire,
  isLoading:state.app.isLoading
});

const mapDispatchToProps = (dispatch) => ({
  getInitialQuestionnaire: () => dispatch(getInitialQuestionnaire())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
