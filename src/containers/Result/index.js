import Result from './Result';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';


const mapStateToProps = (state) => ({
  answers:state.app.answers
});

export default withRouter(connect(mapStateToProps, null)(Result));