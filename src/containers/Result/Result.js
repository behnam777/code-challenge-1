import React from 'react';
import PropTypes from 'prop-types';
import ReactEcharts from 'echarts-for-react';
import './Result.css';
class Result extends React.Component {
  static propTypes = {
    answers: PropTypes.object
  };

  state = {
    answers: []
  };

  componentDidMount() {
    //if no answer is detected, redirect to /.
    if (!this.props.answers || Object.keys(this.props.answers).length === 0) {
      this.props.history.replace('/')
    }

    let answers = [];
    for (let key in this.props.answers) {
      if (this.props.answers.hasOwnProperty(key)) {
        answers.push({name: key, count: this.props.answers[key]})
      }
    }
    this.setState({answers})
  }


  /**
   * generate options for eChart
   * @returns {{radar: [*], series: [*]}}
   */
  getOption = () => {
    let option = {
      radar: [
        {
          indicator: this.state.answers.map(item => ({text: item.name, max: 10})),
          center: ['50%', '50%'],
          radius: 120,
          startAngle: 90,
          shape: 'circle',
          name: {
            formatter: '【{value}】',
            textStyle: {
              color: '#72ACD1'
            }
          },
          splitArea: {
            areaStyle: {
              color: ['rgba(114, 172, 209, 0.2)',
                'rgba(114, 172, 209, 0.4)', 'rgba(114, 172, 209, 0.6)',
                'rgba(114, 172, 209, 0.8)', 'rgba(114, 172, 209, 1)'],
              shadowColor: 'rgba(0, 0, 0, 0.3)',
              shadowBlur: 10
            }
          },
          axisLine: {
            lineStyle: {
              color: 'rgba(255, 255, 255, 0.5)'
            }
          },
          splitLine: {
            lineStyle: {
              color: 'rgba(255, 255, 255, 0.5)'
            }
          }
        }
      ],
      series: [
        {
          name: 'results',
          type: 'radar',
          itemStyle: {
            emphasis: {
              lineStyle: {
                width: 4
              }
            }
          },
          data: [
            {
              label: {
                normal: {
                  show: true,
                  formatter: function (params) {
                    return params.value;
                  }
                }
              },

              value: this.state.answers.map(item => item.count),
              symbol: 'rect',
              symbolSize: 5,
              lineStyle: {
                normal: {
                  type: 'dashed'
                }
              }
            }
          ]
        },
      ]
    };
    return option;
  };

  render() {
    return (
      <div className="chart-container">
        <b>Test Result</b>
        <div className="radar-chart">
          <ReactEcharts
            option={this.getOption()}
            notMerge={true}
            lazyUpdate={true}/>
        </div>
      </div>
    );
  }
}

export default Result;