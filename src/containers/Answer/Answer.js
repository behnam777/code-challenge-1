import React from 'react';
import PropTypes from 'prop-types';
import './Answer.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Answer extends React.Component {
  static propTypes = {
    questionnaire: PropTypes.array,
    headlines: PropTypes.array,
    firstQuestion: PropTypes.object,
    secondQuestion: PropTypes.object,
    getQuestion: PropTypes.func.isRequired,
    removeQuestionFromQuestionnaire: PropTypes.func.isRequired,
    submitAnswer: PropTypes.func.isRequired,
    isFinished: PropTypes.bool,
    totalOptions: PropTypes.number
  };

  static defaultProps = {
    firstQuestion: {},
    secondQuestion: {}
  };

  state = {
    //selected option which can be one of 'first' or 'second'
    selectedOption: '',
    //first option object
    firstQuestion: {},
    //second option object
    secondQuestion: {},
    //percent of progress
    progress: 0
  };

  /**
   * On component mount check availability of questionnaire
   */
  componentDidMount() {
    //if questionnaire not available, redirect to /
    if (this.props.questionnaire.length === 0 || this.props.headlines.length === 0) {
      this.props.history.replace('/');

      //if questionnaire available, get first question
    } else {
      this.props.getQuestion(this.props.questionnaire, this.props.headlines);
    }
  };

  /**
   * on fetching new questions, remove current question from questionnaire
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.firstQuestion && nextProps.firstQuestion !== this.props.firstQuestion && nextProps.secondQuestion && nextProps.secondQuestion !== this.props.secondQuestion) {
      this.props.removeQuestionFromQuestionnaire(nextProps.firstQuestion, nextProps.secondQuestion);
    }
  };

  /**
   * set the selected answer to answers
   */
  handleNextQuestion = () => {
    //is any item selected?
    if (this.state.selectedOption !== '') {
      switch (this.state.selectedOption) {
        case 'first':
          this.props.submitAnswer(this.props.firstQuestion.name);
          break;
        case 'second':
        default:
          this.props.submitAnswer(this.props.secondQuestion.name);
          break;
      }
      //clear selected option for the next question to come
      //increase progress by desired percent
      this.setState({selectedOption: '', progress: this.state.progress + ((100 / this.props.totalOptions) * 2)});
      //get the next question
      this.props.getQuestion(this.props.questionnaire, this.props.headlines);
    }
  };

  /**
   * set selected option
   * @param option
   */
  handleSelect = (option) => {
    this.setState({selectedOption: option})
  };

  /**
   * redirects to result page
   */
  handleShowResult = () => {
    this.props.history.push('/result');
  };

  /**
   * returns the class name of the question
   * if item is selected, adds 'selected' className
   * @param className
   * @returns {*}
   */
  getQuestionClassName = (className) => {
    if (className === this.state.selectedOption) {
      return 'Answer-question selected'
    }
    return 'Answer-question'
  };

  /**
   * render questions or the message for result
   * @returns {XML}
   */
  renderQuestions = () => {
    if (!this.props.isFinished) {
      return (
        <div>
          <p>
            Select one of the following options which do you agree more
          </p>
          <ReactCSSTransitionGroup
            transitionName="question"
            transitionAppear={true}
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}
            transitionAppearTimeout={500}>
            <div className={this.getQuestionClassName('first')} onClick={() => this.handleSelect('first')}>
              <span>{this.props.firstQuestion.question}</span>
            </div>
            <div className={this.getQuestionClassName('second')} onClick={() => this.handleSelect('second')}>
              <span>{this.props.secondQuestion.question}</span>
            </div>
          </ReactCSSTransitionGroup>
        </div>
      );
    } else {
      return (
        <div>
          <p>
            click the below button to see the results
          </p>
        </div>
      );
    }
  };

  /**
   * render Next button based on isFinished prop
   * @returns {XML}
   */
  renderNextButton = () => {
    if (!this.props.isFinished) {
      return (
        <button onClick={this.handleNextQuestion}>Next</button>
      );
    } else {
      return (
        <button onClick={this.handleShowResult}>Result</button>
      );
    }
  };

  render() {
    return (
      <div className="Answer-header">
        <div className="progress-bar-container">
          <span className="progress-bar" style={{width: `${this.state.progress}%`}}/>
        </div>
        {this.renderQuestions()}
        {this.renderNextButton()}
      </div>
    )
  }
}

export default Answer;