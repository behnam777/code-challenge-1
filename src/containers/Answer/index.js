import Answer from './Answer';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {
  getQuestion,
  removeQuestionFromQuestionnaire,
  submitAnswer
} from '../../store/reducers/app';


const mapStateToProps = (state) => ({
  questionnaire: state.app.questionnaire,
  headlines: state.app.headlines,
  firstQuestion: state.app.currentChoice.first,
  secondQuestion: state.app.currentChoice.second,
  isFinished:state.app.isFinished,
  totalOptions:state.app.totalOptions
});

const mapDispatchToProps = (dispatch) => ({
  getQuestion: (questionnaire, headlines) => dispatch(getQuestion(questionnaire, headlines)),
  removeQuestionFromQuestionnaire: (questionA,questionB) => dispatch(removeQuestionFromQuestionnaire(questionA,questionB)),
  submitAnswer: (answer) => dispatch(submitAnswer(answer))
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Answer));